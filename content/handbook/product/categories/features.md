---
title: "Features by Group"
---

## Features by Group

This page is meant to showcase the features by tier across GitLab's Product Hierarchy.

{{% categories-features %}}
